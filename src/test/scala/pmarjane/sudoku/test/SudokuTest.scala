package pmarjane.sudoku.test

import org.scalatest.FunSuite
import pmarjane.sudoku.Sudoku

/**
 * Created by Patrik on 30.6.2014.
 */
class SudokuTest extends FunSuite {
  val fileName: String = "./src/test/resources/sudoku.txt"
  val fileNameDebug: String = "./src/test/resources/sudoku_debug.txt"
  val fileNameSolved: String = "./src/test/resources/sudoku_solved.txt"
  val fileNameEasy: String = "./src/test/resources/sudoku_easy.txt"
  val sArray: Array[Array[Int]] = Sudoku.readSudokuFromFile(fileName)
  val sArrayDebug: Array[Array[Int]] = Sudoku.readSudokuFromFile(fileNameDebug)
  val sArraySolved: Array[Array[Int]] = Sudoku.readSudokuFromFile(fileNameSolved)
  val sArrayEasy: Array[Array[Int]] = Sudoku.readSudokuFromFile(fileNameEasy)

  test("Should be able to read a sudoku from resources") {
    val sudoku = Sudoku.readSudokuFromFile(fileName)
    assert(sudoku.size == 9)
    assert(sudoku(0).size == 9)
  }

  test("Should be able to create a sudoku class") {
    val sudoku = Sudoku(sArray)
    assert(sudoku.sudoku.flatten.toList == sArray.flatten.toList)
  }

  test("Should be able to get rows from sudoku") {
    val sudoku = Sudoku(sArray)
    for (i <- 0 to 8) {
      assert(sudoku.getRow(i).toList == sArray(i).toList)
    }
  }

  test("Should be able to get columns from sudoku") {
    val sudoku = Sudoku(sArray)
    for (i <- 0 to 8) {
      assert(sudoku.getCol(i).toList == sArray.map(x => x(i)).toList)
    }
  }

  test("Should be able to extract blocks from sudoku") {
    val sudoku = Sudoku(sArray)
    assert(sudoku.getBlock(0).flatten.toList.sorted ==
      List(
        sArray(0)(0), sArray(0)(1), sArray(0)(2),
        sArray(1)(0), sArray(1)(1), sArray(1)(2),
        sArray(2)(0), sArray(2)(1), sArray(2)(2)).sorted)

    assert(sudoku.getBlock(4).flatten.toList.sorted ==
      List(
        sArray(3)(3), sArray(3)(4), sArray(3)(5),
        sArray(4)(3), sArray(4)(4), sArray(4)(5),
        sArray(5)(3), sArray(5)(4), sArray(5)(5)).sorted)
  }

  test("Should be able to get block index by x and y") {
    val sudoku = Sudoku(sArray)
    val block = sudoku.getBlockByXAndY(3, 3)
    assert(sudoku.getBlock(4).toList == block.toList)

    val block2 = sudoku.getBlockByXAndY(8, 8)
    assert(sudoku.getBlock(8).toList == block2.toList)
  }

  test("Should give the possible numbers for that cell") {
    val sudoku = Sudoku(sArray)
    val possible = sudoku.possibleNumbers(0, 2)
    assert(possible.toList == List(1, 2, 4))

    val possible2 = sudoku.possibleNumbers(8, 6)
    assert(possible2.toList == List(1, 3, 4, 6))

    val possible3 = sudoku.possibleNumbers(0, 0)
    assert(possible3.toList == List(5))

    val sudokuDebug = Sudoku(sArrayDebug)
    val possible4 = sudokuDebug.possibleNumbers(7,7)
    println(possible4)
  }

  test("Should be able to get a possible numbers map of the whole sudoku") {
    val sudoku = Sudoku(sArray)
    val possibleMap = sudoku.getPossibleNumbersMap
    assert(possibleMap.get((0, 0)).get == Vector(5))
    assert(possibleMap.get((3, 1)).get == Vector(1, 2, 5))
  }

  test("Should be able to tell if a sudoku is valid") {
    val sudoku = Sudoku(sArray)
    assert(sudoku.isValid)
    assert(!sudoku.createNewSudoku((1,1), 3).isValid)
  }

  test("Should know when a sudoku is solved") {
    val sudoku = Sudoku(sArraySolved)
    assert(sudoku.isSolved)
  }

  test("Should be able to solve a sudoku") {
    Sudoku.solveSudoku(fileNameEasy)
  }

}
