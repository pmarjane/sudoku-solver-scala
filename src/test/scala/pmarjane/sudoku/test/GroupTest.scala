package pmarjane.sudoku.test

import org.scalatest.FunSuite
import pmarjane.sudoku.Group

/**
 * Created by Patrik on 1.7.2014.
 */
class GroupTest extends FunSuite {

  test("Group should identify groups with duplicate non-zero elements") {
    val v1 = Vector(0,0,1,2,3,4,5,9)
    val g1 = new Group(v1)
    assert(!g1.hasDuplicates)

    val v2 = Vector(0,0,1,2,2,3,4,5,5)
    val g2 = new Group(v2)
    assert(g2.hasDuplicates)
  }

}
