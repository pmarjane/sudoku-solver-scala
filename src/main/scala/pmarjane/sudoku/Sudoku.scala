package pmarjane.sudoku

import scala.collection.immutable
import scala.io.Source

/**
 * Created by Patrik on 30.6.2014.
 */
object Sudoku {

  def solveSudoku(filename: String) = {
    def innerLoop(sudoku: Sudoku): Sudoku = {
      val map = sudoku.getPossibleNumbersMap
      println(map)
      if (sudoku.isSolved) {
        println("Sudoku is solved:")
        println(sudoku)
      } else {
        val sortedMap = map.toSeq.sortBy(_._2.length)
        val numbersWithMoreThanOnePossibility = sortedMap.dropWhile(_._2.length < 2)
        if (numbersWithMoreThanOnePossibility.isEmpty || sortedMap.head._2.isEmpty) {
          println("No candidates for further recursion")
          println(sortedMap)
        } else {
          val first = numbersWithMoreThanOnePossibility.head
          val index = first._1
          for (n <- first._2) {
            val newSudoku: Sudoku = sudoku.createNewSudoku(index, n)
            println("First: " + first)
            println(newSudoku)
            println(newSudoku.isValid + " : " + newSudoku.isSolved)
            innerLoop(newSudoku)
          }
        }
      }
      sudoku
    }
    val sudoku = Sudoku(readSudokuFromFile(filename))
    innerLoop(sudoku)
  }

  def readSudokuFromFile(name: String): Array[Array[Int]] = {
    val string = Source.fromFile(name).mkString
    string.split("\n").map(_.trim).map(x => x.toCharArray.map(char => Integer.parseInt(char.toString)))
  }

  def apply(sudoku: Array[Array[Int]]) = new Sudoku(sudoku)

  def main(args: Array[String]) = {
    println(solveSudoku("./src/main/resources/sudoku.txt"))
  }

}

class Sudoku(val sudoku: Array[Array[Int]]) {
  def isSolved: Boolean = {
    sudoku.flatten.find(_ == 0).isEmpty && isValid
  }

  def createNewSudoku(newValue: ((Int, Int), Int)): Sudoku = {
    val sudokuVector = sudoku.map(_.toVector).toVector
    val (x, y) = newValue._1
    val newRow = sudokuVector(x).updated(y, newValue._2)
    val newSudoku = sudokuVector.updated(x, newRow)
    Sudoku(newSudoku.map(_.toArray).toArray)
  }

  def getRow(n: Int) = sudoku(n)

  def getCol(n: Int) = sudoku.map(x => x(n))

  /**
   * Block is a three by three square in the sudoku. Indexing starts from 0 (top-left block) to 8 (bottom-right block)
   * Block indexes are read from left to right, ie. block #3 is left-middle block.
   * @param n
   */
  def getBlock(index: Int) = {
    def calculateIndexes(n: Int) = n match {
      case 0 => Vector(0, 1, 2)
      case 1 => Vector(3, 4, 5)
      case 2 => Vector(6, 7, 8)
    }

    val yIndexes = calculateIndexes(index % 3)
    val xIndexes = calculateIndexes((index / 3).toInt)

    val rows = xIndexes.map(x => sudoku(x))
    yIndexes.map(y => rows.map(row => row(y)))
  }

  def isValid = {
    val duplicateSeq = for(i <- 0 to 8) yield {
      val rowIsValid = new Group(getRow(i)).hasDuplicates
      val colIsValid = new Group(getCol(i)).hasDuplicates
      val blockIsValid = new Group(getBlock(i).flatten).hasDuplicates
      Vector(rowIsValid, colIsValid, blockIsValid)
    }

    duplicateSeq.flatten.find(_ == true).isEmpty
  }

  def possibleNumbers(x: Int, y: Int) = {
    val existingNumber = sudoku(x)(y)
    if (existingNumber != 0) {
      Vector(existingNumber)
    } else {
      val row = getRow(x)
      val col = getCol(y)
      val block = getBlockByXAndY(x, y).flatten

      val allNonZeros = (row ++ col ++ block).filter(_ != 0)
      (1 to 9).filter(!allNonZeros.contains(_))
    }
  }

  private def orderMapByValueSize[A, B, C](map: Map[(A, B), IndexedSeq[C]]) = {
    map.toSeq.sortBy(_._2.length)
  }

  def getPossibleNumbersMap = {
    (for(i <- 0 to 8; j <- 0 to 8) yield (i, j) -> possibleNumbers(i, j)).toMap
  }

  def getBlockByXAndY(x: Int, y: Int) = {
    def getBlockIndex(index: Int) = {
      if (Vector(0, 1, 2).contains(index)) Vector(0, 1, 2)
      else if (Vector(3, 4, 5).contains(index)) Vector(3, 4, 5)
      else Vector(6, 7, 8)
    }
    val rows = getBlockIndex(x)
    val cols = getBlockIndex(y)

    val rowValues = rows.map(r => sudoku(r))
    cols.map(c => rowValues.map(r => r(c)))
  }

  override def toString = {
    val sb = new StringBuilder
    for(i <- 0 to 8; j <- 0 to 8) {
      if (j == 0) {
        sb.append("\n")
      }
      sb.append(sudoku(i)(j))
    }
    sb.toString()
  }

}

class Group(val numbers: IndexedSeq[Int]) {

  def hasDuplicates = {
    val noZeros = numbers.filter(_ != 0)
    noZeros.toSet.toVector.sorted != noZeros.toVector.sorted
  }

}
